from io import BytesIO
import requests

from app.config import S3_BUCKET_NAME
from app.s3_storage import S3Storage


def retrieve_byte_object_by_url(url: str) -> BytesIO:
    response = requests.get(url)
    response.raise_for_status()
    return BytesIO(response.content)


def process_audio_export(audio, file_name: str, audio_format: str) -> None:
    storage = S3Storage(S3_BUCKET_NAME)
    io_buffer = BytesIO()
    audio.export(io_buffer, audio_format)
    storage.save(file_name, io_buffer.getvalue())
