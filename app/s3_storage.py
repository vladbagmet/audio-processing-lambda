import contextlib
import io

from botocore.exceptions import ClientError
import boto3
import pickle


class S3Storage:
    def __init__(self, bucket_name):
        self.bucket_name = bucket_name
        self.s3 = boto3.resource('s3')
        self.bucket = self.s3.Bucket(bucket_name)

    def save(self, file_name, value):
        value = pickle.dumps(value)
        return self.bucket.put_object(Key=file_name, Body=value)

    def retrieve(self, file_name, default=None):
        with io.BytesIO() as stream:
            try:
                self.bucket.download_fileobj(file_name, stream)
            except ClientError as err:
                code = err.response.get('Error', {}).get('Code', 'Unknown')
                if code == '404':
                    return default
                else:
                    raise
            stream.seek(0)
            data = stream.read()
        return pickle.loads(data)

    def delete(self, key):
        return self.s3.Object(self.bucket_name, key).delete()
