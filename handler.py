import pydub

from app.utils import process_audio_export, retrieve_byte_object_by_url
from app.config import FFMPEG_BIN_PATH


def process_audio_recording(event, context):
    audio_recording_url = event['url']
    default_audio_format = 'mp3'
    pydub.AudioSegment.converter = FFMPEG_BIN_PATH

    # Retrieve wav file
    byte_object = retrieve_byte_object_by_url(audio_recording_url)
    audio_recording = pydub.AudioSegment.from_wav(byte_object)

    # Split to mono channels
    mono_channels = audio_recording.split_to_mono()
    process_audio_export(audio=mono_channels[0], file_name='mono_left.mp3', audio_format=default_audio_format)
    process_audio_export(audio=mono_channels[1], file_name='mono_right.mp3', audio_format=default_audio_format)

    # Trim audio recording
    first_15_seconds = audio_recording[:15000]
    process_audio_export(audio=first_15_seconds, file_name='first_15_seconds.mp3', audio_format=default_audio_format)

    # Combine 2 recordings
    combined: pydub.AudioSegment = mono_channels[0].append(mono_channels[1], crossfade=5000)
    process_audio_export(
        audio=combined,
        file_name='combined_recording_5seconds_silence.mp3',
        audio_format=default_audio_format
    )

    # Merge to mono
    mono = audio_recording.set_channels(channels=1)
    process_audio_export(audio=mono, file_name='converted_to_mono.mp3', audio_format=default_audio_format)

    # Reading from file
    '''
    Since no writable file system is attached to lambda function on AWS, skipping this test.
    Reading from byte-objects(AudioSegment.from_wav) works as expected.
    '''

    # Converting to OGG
    process_audio_export(audio=audio_recording, file_name='converted_to_ogg.ogg', audio_format='ogg')

    return {
        'statusCode': 200,
        'body': 'processing complete'
    }
