# Audio Processing Lambda
Demo how to work with pydub lib inside AWS lambda function.

Contains:
* functionality to retrieve records by URL;
* audio transformations code;
* configs for AWS deployment;
* config for auto-including ffmpeg as additional layer for AWS lambda function.

## Prerequisites
* Installed npm
* Installed serverless `npm install -g serverless@1.69.0`

## Setting Project Up
* `npm install`
* export ACCESS_KEY_ID={{aws_token}}
* export SECRET_ACCESS_KEY={{aws_secret}}
This is simplified approach of setting up credentials. 
Bucket with name matching `app.config.S3_BUCKET_NAME` should be created forehead.

## Deploying
* `sls deploy`
This deploys to dev environment. Additional production config is required.

## Local Invocations
* set correct path of ffmpeg path on your local machine into config file `app.config.FFMPEG_BIN_PATH`
* sls invoke local -f process_audio -d '{ "url": "https://api.twilio.com/.....recording.wav" }'

## Additional Details
Ffmpeg bin source: `https://johnvansickle.com/ffmpeg/builds/ffmpeg-git-amd64-static.tar.xz`
To use another bin, put it inside `layer/ffmpeg` folder and make executable.
